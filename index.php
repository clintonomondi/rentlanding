<?php
require_once 'navbar.php'
?>


<div class="home-slider-area">
    <div class="container-fluid m-0 p-0">
        <div class="home-slider owl-carousel owl-theme" data-slider-id="1">
            <div class="slider-item">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="home-slider-content">
                            <span>GikHut</span>
                            <h1>Modernize your business  <b>with one powerful platform</b></h1>
                            <p>Finally, property management software that gives you the clarity to focus on what matters most. </p>
                            <div class="home-slider-btn">
                                <a href="https://account.gikhut.com/" class="default-btn">
                                    Create Account
                                    <i class='bx bx-right-arrow-alt'></i>
                                </a>
                                <a href="https://account.gikhut.com/" class="default-btn active">
                                    Login
                                    <i class='bx bx-right-arrow-alt'></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 pr-0">
                        <div class="home-slider-img">
                            <img src="/images/slider2.jpg" alt="Images">
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-item">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="home-slider-content">
                            <span>GikHut</span>
                            <h1>SIMPLIFY. AUTOMATE. <b>GROW</b></h1>
                            <p>The highest rated and easiest to learn property management software that includes free 24hrs-based support. </p>
                            <div class="home-slider-btn">
                                <a href="https://account.gikhut.com/" class="default-btn">
                                    Create Account
                                    <i class='bx bx-right-arrow-alt'></i>
                                </a>
                                <a href="https://account.gikhut.com/" class="default-btn active">
                                    Login
                                    <i class='bx bx-right-arrow-alt'></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 pr-0">
                        <div class="home-slider-img">
                            <img src="/images/slider1.jpg" alt="Images">
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-item">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="home-slider-content">
                            <span>GikHut</span>
                            <h1>Manage Your Lovely & <b>Comfort Home</b></h1>
                            <p>Intuitively designed to delight your residents and staff </p>
                            <div class="home-slider-btn">
                                <a href="https://account.gikhut.com/" class="default-btn">
                                    Create Account
                                    <i class='bx bx-right-arrow-alt'></i>
                                </a>
                                <a href="https://account.gikhut.com/" class="default-btn active">
                                    Login
                                    <i class='bx bx-right-arrow-alt'></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 pr-0">
                        <div class="home-slider-img">
                            <img src="assets/img/home1/2.jpg" alt="Images">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="thumbs-wrap">
        <div class="owl-thumbs home-slider-thumb" data-slider-id="1">
            <div class="owl-thumb-item">
                <span>01</span>
            </div>
            <div class="owl-thumb-item">
                <span>02</span>
            </div>
            <div class="owl-thumb-item">
                <span>03</span>
            </div>
        </div>
    </div>

</div>


<div class="service-area pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="service-card service-card-bg">
                    <i class='flaticon-bankrupt'></i>
                    <a href="https://account.gikhut.com">
                        <h3>Property Management</h3>
                    </a>
                    <p class="text-break">A complete management solution for professional property managers.</p>
                    <a href="#" class="learn-more-btn">
                        Sign up
                        <i class='bx bx-right-arrow-alt'></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="service-card service-card-bg active">
                    <i class='flaticon-value'></i>
                    <a href="https://account.gikhut.com">
                        <h3>Tenant Management</h3>
                    </a>
                    <p class="text-break">Order instant credit, criminal, and eviction reports on your tenants.
                       </p>
                    <a href="https://account.gikhut.com" class="learn-more-btn">
                        Sign up
                        <i class='bx bx-right-arrow-alt'></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="service-card service-card-bg">
                    <i class='flaticon-time-management'></i>
                    <a href="https://account.gikhut.com">
                        <h3>Landlords</h3>
                    </a>
                    <p class="text-break">All the tools for landlords to easily manage properties and tenants.</p>
                    <a href="https://account.gikhut.com" class="learn-more-btn">
                        Sign up
                        <i class='bx bx-right-arrow-alt'></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="service-card service-card-bg">
                    <i class='flaticon-house'></i>
                    <a href="https://account.gikhut.com">
                        <h3>Rental Invoice</h3>
                    </a>
                    <p class="text-break">Automate invoice to tenants and other charges to tenants within your premises.</p>
                    <a href="https://account.gikhut.com" class="learn-more-btn">
                        Sign up
                        <i class='bx bx-right-arrow-alt'></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="property-area pt-100 pb-70">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-5 pl-0">
                <div class="property-img">
                    <a href="property-details.html">
                        <img src="/images/pic1.jpg" alt="Images">
                    </a>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="property-item">
                    <div class="section-title">
                        <span>RESIDENTIAL</span>
                        <h2>
                            <a href="property-details.html">
                                Minimalism In Style
                                <b>Your Property</b>
                            </a>
                        </h2>
                        <p>
                            Manage your entire residential portfolio on one intuitive platform.
                            With AppFolio your team can do anything from anywhere in a fully digital environment. Streamlined processes and smart
                            automation free your team to focus on your communities and deliver an outstanding resident experience
                        </p>
                    </div>
                    <div class="property-counter">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-md-3">
                                <div class="counter-card">
                                    <h2>120</h2>
                                    <h3>Property</h3>
                                    <span>Onboard</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-md-3">
                                <div class="counter-card">
                                    <h2>2,005</h2>
                                    <h3>Tenants</h3>
                                    <span>Onboard</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-md-3">
                                <div class="counter-card">
                                    <h2>1,033</h2>
                                    <h3>Invoice</h3>
                                    <span>This month</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-md-3">
                                <div class="counter-card">
                                    <h2>24/7</h2>
                                    <h3>SUPPORT</h3>
                                    <span>Guaranteed</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<div class="project-area project-bg1">
    <div class="container">
        <div class="project-card">
            <span>NEW PROJECT IN PROGRESS</span>
            <h2>751 Vilkoma Street View Luxury Project</h2>
            <ul>
                <li>
                    <b>PROPERTY SIZE:</b>
                    700sq ft
                </li>
                <li>
                    <b>START DATE:</b>
                    July 12th 2020
                </li>
                <li>
                    <b>FINISH DATE:</b>
                    July 12th 2021
                </li>
                <li>
                    <b>INVESTOR:</b>
                    Jaeuin Group Inc
                </li>
            </ul>
            <div class="project-card-btn">
                <a href="property-details.html" class="default-btn default-bg-buttercup">View Details <i class='bx bx-right-arrow-alt'></i></a>
                <a href="#" class="default-btn default-regal-blue active">Download Brochure <i class='bx bx-right-arrow-alt'></i></a>
            </div>
        </div>
    </div>
</div>


<div class="orgin-area pt-100 pb-70">
    <div class="container-fluid">
        <div class="container-max">
            <div class="orgin-title">
                <div class="section-title">
                    <span>power resources</span>
                    <h2>Enough Energy<b>Origin</b> </h2>
                    <p>
                        Lorem ipsum dolor sit ame consectetur adipisicing elit, sed do eiusmod tempor incididunt
                        ut labore et dolore magna aliquaUt enim ad minim veniaquis nostrud exercitation
                    </p>
                </div>
            </div>
            <div class="row pt-45">
                <div class="col-lg-3 col-sm-6">
                    <div class="orgin-card">
                        <h2>60%</h2>
                        <h3>Project 03 Due Date 2021</h3>
                        <p>
                            Details ipsum dolor sitameLorem adip
                            iscing cnsectetur adipiscing mod
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="orgin-card">
                        <h2>50%</h2>
                        <h3>Project 14 Due Date 2031</h3>
                        <p>
                            Details ipsum dolor sitameLorem adip
                            iscing cnsectetur adipiscing mod
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="orgin-card">
                        <h2>30%</h2>
                        <h3>Project 12 Due Date 2021</h3>
                        <p>
                            Details ipsum dolor sitameLorem adip
                            iscing cnsectetur adipiscing mod
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="orgin-card">
                        <h2>20%</h2>
                        <h3>Project 16 Due Date 2021</h3>
                        <p>
                            Details ipsum dolor sitameLorem adip
                            iscing cnsectetur adipiscing mod
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="innovation-area pb-70">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <div class="innovation-content">
                    <div class="section-title">
                        <span>DELIVERING INNOVATION</span>
                        <h2>Sustainability Property <b>Goals As Expected</b></h2>
                        <p>
                            Lorem ipsum dolor sit ame consectetur adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliquaUt enim ad minim vequis nostrud exercitation
                        </p>
                    </div>
                    <div class="innovation-btn">
                        <a href="#" class="default-btn default-bg-buttercup">View Details <i class='bx bx-right-arrow-alt'></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="innovation-bg">
                    <div class="innovation-slider owl-carousel owl-theme">
                        <div class="innovation-item">
                            <i class='flaticon-smartphone'></i>
                            <h3>Quality Management</h3>
                            <p>
                                Lorem ipsum doconsectetur adipisicing elit sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.Ut eveniam
                            </p>
                        </div>
                        <div class="innovation-item">
                            <i class='flaticon-growth'></i>
                            <h3>Designed Marvel</h3>
                            <p>
                                Lorem ipsum doconsectetur adipisicing elit sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.Ut eveniam
                            </p>
                        </div>
                        <div class="innovation-item">
                            <i class='flaticon-smartphone'></i>
                            <h3>Quality Management</h3>
                            <p>
                                Lorem ipsum doconsectetur adipisicing elit sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.Ut eveniam
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="efficiency-area pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="efficiency-card">
                    <span>EFFICIENCY ENERGY</span>
                    <a href="service-details.html">
                        <h3>Heating and Cooling You Need Realy this</h3>
                    </a>
                    <p>Lorem ipsum dolor sit amet, consectetur isicing elit, est, qui dolorem ipsum</p>
                    <i class='flaticon-buildings'></i>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="efficiency-card">
                    <span>EFFICIENCY ENERGY</span>
                    <a href="service-details.html">
                        <h3>Ventilation Is Superior Support For Property</h3>
                    </a>
                    <p>Lorem ipsum dolor sit amet, consectetur isicing elit, est, qui dolorem ipsum</p>
                    <i class='flaticon-buildings'></i>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 offset-md-3 offset-lg-0">
                <div class="efficiency-card">
                    <span>EFFICIENCY ENERGY</span>
                    <a href="service-details.html">
                        <h3>Glazing system You Need Realy this</h3>
                    </a>
                    <p>Lorem ipsum dolor sit amet, consectetur isicing elit, est, qui dolorem ipsum</p>
                    <i class='flaticon-buildings'></i>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="contact-area">
    <div class="container">
        <div class="contact-option">
            <div class="contact-form">
                <span>SEND MESSAGE</span>
                <h2>Contact With Us</h2>
                <form id="contactForm">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            <div class="form-group">
                                <i class='bx bx-user'></i>
                                <input type="text" name="name" id="name" class="form-control" required data-error="Please enter your name" placeholder="Your Name*">
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="form-group">
                                <i class='bx bx-user'></i>
                                <input type="email" name="email" id="email" class="form-control" required data-error="Please enter your email" placeholder="Email*">
                            </div>
                        </div>
                        <div class="col-lg-12 col-sm-12">
                            <div class="form-group">
                                <i class='bx bx-phone'></i>
                                <input type="text" name="phone_number" id="phone_number" required data-error="Please enter your number" class="form-control" placeholder="Your Phone">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <i class='bx bx-envelope'></i>
                                <textarea name="message" class="form-control" id="message" cols="30" rows="8" required data-error="Write your message" placeholder="Your Message"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <button type="submit" class="default-btn default-bg-buttercup">
                                Send Message
                                <i class='bx bx-right-arrow-alt'></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>





<div class="newsletter-area pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="newsletter-content">
                    <i class='flaticon-email'></i>
                    <h2>Join our weekly <b>Newsletter</b></h2>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="newsletter-form-area">
                    <form class="newsletter-form" data-toggle="validator" method="POST">
                        <input type="email" class="form-control" placeholder="Your Email*" name="EMAIL" required autocomplete="off">
                        <button class="default-btn" type="submit">
                            Subscribe
                            <i class='bx bx-right-arrow-alt'></i>
                        </button>
                        <div id="validator-newsletter" class="form-result"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="map-area">
    <div class="container-fluid m-0 p-0">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1887.3734131639715!2d-96.95588917878352!3d18.89830951964275!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85c4e51eb45eacad%3A0x465ac54aa2735573!2zUmluY29uIGRlbCBCb3NxdWUsIOCmleCmsOCnjeCmoeCni-CmrOCmviwg4Kat4KeH4Kaw4Ka-4KaV4KeN4Kaw4KeB4KacLCDgpq7gp4fgppXgp43gprjgpr_gppXgp4s!5e0!3m2!1sbn!2sbd!4v1594641366896!5m2!1sbn!2sbd" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        <div class="map-content">
            <span>CONTACT FOR PROJECT</span>
            <h2>Do You Have A Project In <b>Mind?</b></h2>
            <div class="map-content-left">
                <span>CALL US NOW</span>
                <h3><a href="tel:+5678555178">+5 678 555 178</a></h3>
            </div>
            <div class="map-content-right">
                <span>GET IN TOUCH</span>
                <h3><a href="https://templates.hibootstrap.com/cdn-cgi/l/email-protection#c1a8afa7ae81aea7b5aeb1efa2aeac"><span class="__cf_email__" data-cfemail="11787f777e517e77657e613f727e7c">[email&#160;protected]</span></a></h3>
            </div>
        </div>
    </div>
</div>

<?php
require_once 'footer.php'
?>