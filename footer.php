<footer class="footer-area">
    <div class="footer-top pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single-footer-widget">
                        <a href="index-5.html" class="logo">
                            <img src="assets/img/logos/logo1.png" alt="Logo">
                        </a>
                        <p>
                            Lorem ipsum dolor sit ame consectetur adisicing elitsed do eiusmod
                            tempor labet dolore magna aliquaUt
                        </p>
                        <ul class="social-link">
                            <li>
                                <a href="#" target="_blank"><i class='bx bxl-facebook'></i></a>
                            </li>
                            <li>
                                <a href="#" target="_blank"><i class='bx bxl-twitter'></i></a>
                            </li>
                            <li>
                                <a href="#" target="_blank"><i class='bx bxl-instagram'></i></a>
                            </li>
                            <li>
                                <a href="#" target="_blank"><i class='bx bxl-pinterest-alt'></i></a>
                            </li>
                            <li>
                                <a href="#" target="_blank"><i class='bx bxl-youtube'></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="single-footer-widget">
                        <h3>SERVICES</h3>
                        <ul class="footer-list">
                            <li>
                                <a href="services.html" target="_blank">
                                    <i class='bx bx-plus'></i>
                                    Property on Sale
                                </a>
                            </li>
                            <li>
                                <a href="about.html" target="_blank">
                                    <i class='bx bx-plus'></i>
                                    About Us
                                </a>
                            </li>
                            <li>
                                <a href="team.html" target="_blank">
                                    <i class='bx bx-plus'></i>
                                    Our Team
                                </a>
                            </li>
                            <li>
                                <a href="terms-condition.html" target="_blank">
                                    <i class='bx bx-plus'></i>
                                    Terms of Use
                                </a>
                            </li>
                            <li>
                                <a href="privacy-policy.html" target="_blank">
                                    <i class='bx bx-plus'></i>
                                    Privacy Policy
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer-widget pl-3">
                        <h3>CONTACT INFO</h3>
                        <ul class="footer-contact-list">
                            <li>
                                <span>Monday - Friday :</span> 9 am to 6 pm
                            </li>
                            <li>
                                <span>Saturday - Sunday :</span> 9 am to 2 pm
                            </li>
                            <li>
                                <span>Phone :</span> <a href="tel:2151234567"> 215 - 123 - 4567</a>
                            </li>
                            <li>
                                <span>Email :</span> <a href="https://templates.hibootstrap.com/cdn-cgi/l/email-protection#f29b9c949db2bd94869d829b9c91dc919d9f"> <span class="__cf_email__" data-cfemail="6f060109002f00091b001f06010c410c0002">[email&#160;protected]</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer-widget pl-5">
                        <h3>Gallery</h3>
                        <ul class="footer-gallery">
                            <li>
                                <a href="#">
                                    <img src="assets/img/gallery/g-1.jpg" alt="image">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="assets/img/gallery/g-2.jpg" alt="image">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="assets/img/gallery/g-3.jpg" alt="image">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="assets/img/gallery/g-4.jpg" alt="image">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="assets/img/gallery/g-5.jpg" alt="image">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="assets/img/gallery/g-6.jpg" alt="image">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="bottom-text">
                <p>
                    Copyright @2021 GikHut. All Rights Reserved by
                    <a href="https://giktek.io/" target="_blank">Giktek Limited</a>
                </p>
            </div>
        </div>
    </div>
</footer>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery-3.5.1.slim.min.js"></script>

<script src="assets/js/popper.min.js"></script>

<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/carousel-thumbs.js"></script>

<script src="assets/js/meanmenu.js"></script>

<script src="assets/js/jquery.magnific-popup.min.js"></script>

<script src="assets/js/wow.min.js"></script>

<script src="assets/js/jquery.nice-select.min.js"></script>

<script src="assets/js/jquery.ajaxchimp.min.js"></script>

<script src="assets/js/form-validator.min.js"></script>

<script src="assets/js/contact-form-script.js"></script>

<script src="assets/js/custom.js"></script>
</body>

<!-- Mirrored from templates.hibootstrap.com/oftop/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Jan 2021 09:58:36 GMT -->
</html>