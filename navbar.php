<!doctype html>
<html lang="zxx">

<!-- Mirrored from templates.hibootstrap.com/oftop/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Jan 2021 09:58:36 GMT -->
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="assets/fonts/flaticon.css">

    <link rel="stylesheet" href="assets/css/boxicons.min.css">

    <link rel="stylesheet" href="assets/css/animate.min.css">

    <link rel="stylesheet" href="assets/css/magnific-popup.css">

    <link rel="stylesheet" href="assets/css/meanmenu.css">

    <link rel="stylesheet" href="assets/css/nice-select.min.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <link rel="stylesheet" href="assets/css/responsive.css">

    <title>GikHut-Home</title>

    <link rel="icon" type="image/png" href="assets/img/favicon.png">
</head>
<body>

<div class="preloader">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="spinner">
                <div class="circle1"></div>
                <div class="circle2"></div>
                <div class="circle3"></div>
            </div>
        </div>
    </div>
</div>


<div class="navbar-area">

    <div class="mobile-nav">
        <a href="/" class="logo">
            <img src="/images/logo.jpg" alt="Logo">
        </a>
    </div>

    <div class="main-nav">
        <div class="container-fluid">
            <nav class="container-max navbar navbar-expand-md navbar-light ">
                <a class="navbar-brand" href="index-2.html">
                    <img src="/images/logo.jpg" alt="Logo">
                </a>
                <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item">
                            <a href="/" class="nav-link active">
                                Home
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                About
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                Services
                                <i class='bx bx-chevron-down'></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Property management
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Tenant management
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                Contact
                            </a>
                        </li>
                    </ul>
                    <div class="side-btn-area">
                        <a href="tel:+1(778)453221" class="call-btn">
                            <i class='bx bx-phone'></i>
                            +254 735812001
                        </a>
                    </div>
                    <div class="appointment-btn">
                        <a href="https://account.gikhut.com/" class="default-btn default-bg-buttercup">
                            My Account
                            <i class='bx bx-right-arrow-alt'></i>
                        </a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>







